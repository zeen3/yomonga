/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/
import * as $protobuf from "protobufjs/light";

const $root = ($protobuf.roots["default"] || ($protobuf.roots["default"] = new $protobuf.Root()))
.addJSON({
  updates: {
    nested: {
      UpdatesResponse: {
        fields: {
          titles: {
            rule: "repeated",
            type: "title.Summary",
            id: 1
          },
          isFirstTitleHighlighted: {
            type: "bool",
            id: 2
          }
        }
      },
      UpdatesRequest: {
        fields: {
          day: {
            type: "Day",
            id: 1
          }
        },
        nested: {
          Day: {
            values: {
              MON: 0,
              TUE: 1,
              WED: 2,
              THU: 3,
              FRI: 4,
              SAT: 5,
              SUN: 6
            }
          }
        }
      }
    }
  },
  title: {
    nested: {
      Summary: {
        fields: {
          id: {
            type: "uint32",
            id: 1
          },
          thumbnailUrl: {
            type: "string",
            id: 2
          },
          name: {
            type: "string",
            id: 3
          },
          authorName: {
            type: "string",
            id: 4
          },
          catchCopy: {
            type: "string",
            id: 5
          },
          description: {
            type: "string",
            id: 6
          },
          isRecentlyUpdate: {
            type: "bool",
            id: 101
          },
          bookmarkCount: {
            type: "uint32",
            id: 102
          },
          ticketInfo: {
            type: "common.TicketInfo",
            id: 103
          },
          remainingRentalTime: {
            type: "uint32",
            id: 201
          }
        }
      },
      DetailResponse: {
        fields: {
          id: {
            type: "uint32",
            id: 1
          },
          coverImageUrl: {
            type: "string",
            id: 2
          },
          name: {
            type: "string",
            id: 3
          },
          authorName: {
            type: "string",
            id: 4
          },
          updateInfo: {
            type: "string",
            id: 5
          },
          tags: {
            rule: "repeated",
            type: "common.Tag",
            id: 6
          },
          description: {
            type: "string",
            id: 7
          },
          notification: {
            type: "string",
            id: 8
          },
          bookmarkCount: {
            type: "uint32",
            id: 101
          },
          ticketInfo: {
            type: "common.TicketInfo",
            id: 102
          },
          externalLinks: {
            rule: "repeated",
            type: "ExternalLink",
            id: 103
          },
          chapters: {
            rule: "repeated",
            type: "chapter.Summary",
            id: 200
          },
          currency: {
            type: "common.Currency",
            id: 300
          }
        },
        nested: {
          ExternalLink: {
            fields: {
              description: {
                type: "string",
                id: 1
              },
              imageUrl: {
                type: "string",
                id: 2
              },
              externalLinkUrl: {
                type: "string",
                id: 3
              }
            }
          }
        }
      },
      ChunkResponse: {
        fields: {
          subject: {
            type: "string",
            id: 1
          },
          titles: {
            rule: "repeated",
            type: "Summary",
            id: 2
          }
        }
      },
      Request: {
        fields: {
          titleId: {
            type: "uint32",
            id: 1
          }
        }
      },
      RequestChunk: {
        fields: {
          type: {
            type: "Type",
            id: 1
          },
          id: {
            type: "uint32",
            id: 2
          }
        },
        nested: {
          Type: {
            values: {
              TAG: 0,
              SECTION: 1
            }
          }
        }
      }
    }
  },
  common: {
    nested: {
      Currency: {
        fields: {
          bonusCoins: {
            type: "int32",
            id: 1
          },
          paidCoins: {
            type: "int32",
            id: 2
          }
        }
      },
      MoveTo: {
        fields: {
          title: {
            type: "uint32",
            id: 1
          },
          titlesWithTag: {
            type: "Tag",
            id: 2
          },
          url: {
            type: "string",
            id: 3
          },
          sectionId: {
            type: "uint32",
            id: 4
          }
        }
      },
      Tag: {
        fields: {
          id: {
            type: "uint32",
            id: 1
          },
          name: {
            type: "string",
            id: 2
          }
        }
      },
      TicketInfo: {
        fields: {
          nextRecoveryDate: {
            type: "string",
            id: 1
          }
        }
      }
    }
  },
  chapter: {
    nested: {
      ViewerResponse: {
        fields: {
          detail: {
            type: "Detail",
            id: 1
          },
          error: {
            type: "Error",
            id: 2
          }
        },
        nested: {
          Detail: {
            fields: {
              pages: {
                rule: "repeated",
                type: "Page",
                id: 1
              },
              name: {
                type: "string",
                id: 2
              },
              titleId: {
                type: "uint32",
                id: 3
              },
              isVerticalScrollView: {
                type: "bool",
                id: 4
              }
            },
            nested: {
              Page: {
                fields: {
                  image: {
                    type: "Image",
                    id: 1
                  },
                  last: {
                    type: "Last",
                    id: 2
                  }
                },
                nested: {
                  Image: {
                    fields: {
                      url: {
                        type: "string",
                        id: 1
                      },
                      descriptionKey: {
                        type: "string",
                        id: 2
                      }
                    }
                  },
                  Last: {
                    fields: {
                      next: {
                        type: "Summary",
                        id: 1
                      },
                      recommendedTitles: {
                        rule: "repeated",
                        type: "title.Summary",
                        id: 2
                      },
                      isBookmarked: {
                        type: "bool",
                        id: 101
                      },
                      tickedInfo: {
                        type: "common.TicketInfo",
                        id: 102
                      },
                      currency: {
                        type: "common.Currency",
                        id: 103
                      }
                    }
                  }
                }
              }
            }
          },
          Error: {
            fields: {
              info: {
                type: "Info",
                id: 1
              },
              summary: {
                type: "Summary",
                id: 2
              }
            },
            nested: {
              Info: {
                values: {
                  LOGGIN_NEEDED: 0,
                  NOT_PURCHASED: 1
                }
              }
            }
          }
        }
      },
      Summary: {
        fields: {
          id: {
            type: "uint32",
            id: 1
          },
          thumbnailUrl: {
            type: "string",
            id: 2
          },
          name: {
            type: "string",
            id: 3
          },
          publishDate: {
            type: "string",
            id: 4
          },
          consumption: {
            type: "Consumption",
            id: 5
          },
          price: {
            type: "uint32",
            id: 6
          },
          isRecentlyUpdated: {
            type: "bool",
            id: 102
          },
          purchaseDate: {
            type: "uint32",
            id: 201
          },
          isRead: {
            type: "bool",
            id: 202
          }
        },
        nested: {
          Consumption: {
            values: {
              FREE: 0,
              TICKET: 1,
              COIN: 2,
              ADVANCE: 3
            }
          }
        }
      },
      Request: {
        fields: {
          id: {
            type: "uint32",
            id: 1
          }
        }
      }
    }
  },
  search: {
    nested: {
      SearchResponse: {
        fields: {
          genres: {
            rule: "repeated",
            type: "Genre",
            id: 1
          },
          keywords: {
            rule: "repeated",
            type: "common.Tag",
            id: 2
          }
        },
        nested: {
          Genre: {
            fields: {
              tag: {
                type: "common.Tag",
                id: 1
              },
              iconImageUrl: {
                type: "string",
                id: 2
              }
            }
          }
        }
      },
      ResultResponse: {
        fields: {
          titles: {
            rule: "repeated",
            type: "title.Summary",
            id: 1
          }
        }
      },
      SearchRequest: {
        fields: {
          words: {
            type: "string",
            id: 1
          }
        }
      }
    }
  },
  home: {
    nested: {
      CodeResponse: {
        fields: {
          code: {
            type: "string",
            id: 1
          }
        }
      },
      MyPageResponse: {
        fields: {
          currency: {
            type: "Currency",
            id: 1
          },
          bookmarkedTitles: {
            rule: "repeated",
            type: "title.Summary",
            id: 2
          },
          historyTitles: {
            rule: "repeated",
            type: "title.Summary",
            id: 3
          },
          rewardUrl: {
            type: "string",
            id: 4
          }
        }
      },
      SetFavourite: {
        fields: {
          titleId: {
            type: "uint32",
            id: 1
          }
        }
      },
      HomeResponse: {
        fields: {
          banners: {
            rule: "repeated",
            type: "Banner",
            id: 1
          },
          sections: {
            rule: "repeated",
            type: "Section",
            id: 2
          },
          notification: {
            type: "Notification",
            id: 101
          }
        },
        nested: {
          Banner: {
            fields: {
              imageUrl: {
                type: "string",
                id: 1
              },
              name: {
                type: "string",
                id: 2
              },
              moveTo: {
                type: "common.MoveTo",
                id: 3
              }
            }
          },
          Section: {
            fields: {
              header: {
                type: "string",
                id: 1
              },
              titles: {
                type: "Titles",
                id: 3
              },
              ranking: {
                type: "Ranking",
                id: 4
              },
              singleBanner: {
                type: "Banner",
                id: 5
              },
              loginInduction: {
                type: "LoginInduction",
                id: 10
              },
              moreLink: {
                type: "common.MoveTo",
                id: 6
              },
              bgColour: {
                type: "string",
                id: 7
              },
              subjectAndNameTextColour: {
                type: "string",
                id: 8
              },
              descriptionTextColour: {
                type: "string",
                id: 9
              }
            },
            nested: {
              Ranking: {
                fields: {
                  categories: {
                    rule: "repeated",
                    type: "Category",
                    id: 1
                  }
                },
                nested: {
                  Category: {
                    fields: {
                      name: {
                        type: "string",
                        id: 1
                      },
                      titles: {
                        rule: "repeated",
                        type: "title.Summary",
                        id: 2
                      }
                    }
                  }
                }
              },
              LoginInduction: {
                fields: {
                  imageUrl: {
                    type: "string",
                    id: 1
                  }
                }
              },
              Titles: {
                fields: {
                  titles: {
                    rule: "repeated",
                    type: "title.Summary",
                    id: 1
                  },
                  layout: {
                    type: "Layout",
                    id: 2
                  }
                },
                nested: {
                  Layout: {
                    values: {
                      SMALL: 0,
                      LARGE: 1,
                      SMALL_WITH_TICKET_INFO: 2
                    }
                  }
                }
              },
              Notification: {
                fields: {
                  simpleMessage: {
                    type: "SimpleMessage",
                    id: 1
                  },
                  rewardAd: {
                    type: "RewardAd",
                    id: 2
                  }
                },
                nested: {
                  RewardAd: {
                    fields: {
                      rewardAmount: {
                        type: "uint32",
                        id: 1
                      },
                      url: {
                        type: "string",
                        id: 2
                      }
                    }
                  },
                  SimpleMessage: {
                    fields: {
                      subject: {
                        type: "string",
                        id: 1
                      },
                      body: {
                        type: "string",
                        id: 2
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  },
  ranking: {
    nested: {
      RankingResponse: {
        fields: {
          genres: {
            rule: "repeated",
            type: "Genre",
            id: 1
          }
        },
        nested: {
          Genre: {
            fields: {
              name: {
                type: "string",
                id: 1
              },
              titles: {
                rule: "repeated",
                type: "title.Summary",
                id: 2
              }
            }
          }
        }
      }
    }
  },
  purchase: {
    nested: {
      AquisitionHistoryResponse: {
        fields: {
          entries: {
            rule: "repeated",
            type: "Entry",
            id: 1
          }
        },
        nested: {
          Entry: {
            fields: {
              subject: {
                type: "string",
                id: 1
              },
              date: {
                type: "string",
                id: 2
              },
              paidCoinsAmount: {
                type: "uint32",
                id: 3
              },
              bonusCoinsAmount: {
                type: "uint32",
                id: 4
              }
            }
          }
        }
      },
      PurchaseProductsResponse: {
        fields: {
          currency: {
            type: "common.Currency",
            id: 1
          },
          products: {
            rule: "repeated",
            type: "Product",
            id: 2
          }
        },
        nested: {
          Product: {
            fields: {
              id: {
                type: "uint32",
                id: 1
              },
              amount: {
                type: "uint32",
                id: 2
              },
              bonusAmount: {
                type: "uint32",
                id: 3
              },
              price: {
                type: "uint32",
                id: 4
              },
              url: {
                type: "string",
                id: 5
              }
            }
          }
        }
      },
      PurchaseChapterRequest: {
        fields: {
          chapterId: {
            type: "uint32",
            id: 1
          },
          consumption: {
            type: "Consumption",
            id: 2
          }
        },
        nested: {
          Consumption: {
            values: {
              WAITFREE: 0,
              COIN: 1
            }
          }
        }
      }
    }
  }
});

export { $root as default };
