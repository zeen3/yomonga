const URL: &'static str = "https://yomanga.com/api";
use crate::cat::{
    AquisitionHistoryResponse, ChunkResponse, CodeResponse, DetailResponse, HomeResponse,
    MyPageResponse, PurchaseProductsResponse, RankingResponse, ResultResponse, SearchResponse,
    SubscribingProductsResponse, UpdatesResponse, ViewerResponse,
};
use http::Method;
use quick_protobuf::{BytesReader, MessageRead};
use serde::Serialize;
use std::num::NonZeroU32;
type URL = String;
#[derive(Default, Debug)]
pub struct Req {
    pub method: Method,
    pub url: URL,
    pub body: Option<Vec<u8>>,
}
impl Req {
    pub fn from_url(url: &'static str) -> Self {
        Self {
            method: Method::GET,
            body: None,
            url: format!("{}/{}", URL, url),
        }
    }
    pub fn query<S: Serialize>(mut self, q: S) -> Self {
        let q = serde_url_params::to_string(&q).unwrap();
        if !q.is_empty() {
            self.url.push('?');
            self.url.push_str(&q);
        }
        self
    }
    pub fn method(mut self, method: Method) -> Self {
        self.method = method;
        self
    }
    pub fn body<V8: Into<Vec<u8>>>(self, body: V8) -> Self {
        self.body_opt(Some(body))
    }
    pub fn body_opt<V8: Into<Vec<u8>>>(mut self, body: Option<V8>) -> Self {
        self.body = body.map(|v| v.into());
        self
    }
    pub fn param_body<S: Serialize>(mut self, body: S) -> Self {
        self.body = serde_url_params::to_vec(&body).ok();
        self
    }
}
pub trait ReqRes {
    type Res;
    fn to_req(&self) -> Req;
    fn from_resp<'a>(data: &'a [u8]) -> quick_protobuf::Result<Self::Res>
    where
        Self::Res: MessageRead<'a>,
    {
        let len = data.len();
        let mut bytes = BytesReader::from_bytes(&data);
        bytes.read_message_by_len(&data, len)
    }
}
#[derive(Serialize, Debug, Clone, Copy)]
#[serde(rename_all = "lowercase")]
pub enum Updates {
    Mon,
    Tue,
    Wed,
    Thu,
    Fri,
    Sat,
    Sun,
}
impl ReqRes for Updates {
    type Res = UpdatesResponse;
    fn to_req(&self) -> Req {
        #[derive(Serialize)]
        struct Update {
            day: Updates,
        }
        Req::from_url("updates").query(Update { day: *self })
    }
}
#[derive(Serialize, Debug, Clone)]
pub struct Search {
    pub words: String,
}
impl ReqRes for Search {
    type Res = ResultResponse;
    fn to_req(&self) -> Req {
        Req::from_url("search").query(self)
    }
}
#[derive(Serialize, Debug, Clone, Copy)]
pub struct PurchaseProducts {
    #[serde(rename = "_xuid")]
    pub xuid: Option<NonZeroU32>,
    #[serde(rename = "_article")]
    pub article: Option<NonZeroU32>,
}
impl ReqRes for PurchaseProducts {
    type Res = PurchaseProductsResponse;
    fn to_req(&self) -> Req {
        Req::from_url("purchases_items").query(self)
    }
}

#[derive(Serialize, Debug, Clone, Copy)]
pub struct Title {
    pub title_id: u32,
}
impl ReqRes for Title {
    type Res = DetailResponse;
    fn to_req(&self) -> Req {
        Req::from_url("title").query(self)
    }
}

#[derive(Serialize, Debug, Clone, Copy)]
#[serde(rename_all = "lowercase")]
pub enum ChunkType {
    Tag,
    Section,
    Updated,
    Recommmend,
}
#[derive(Serialize, Debug, Clone, Copy)]
pub struct Chunk {
    #[serde(rename = "type")]
    pub chunk_type: ChunkType,
    pub id: Option<NonZeroU32>,
}
impl ReqRes for Chunk {
    type Res = ChunkResponse;
    fn to_req(&self) -> Req {
        Req::from_url("titles").query(self)
    }
}
#[derive(Serialize, Debug, Clone, Copy)]
pub struct Chapter {
    pub id: u32,
}
impl ReqRes for Chapter {
    type Res = ViewerResponse;
    fn to_req(&self) -> Req {
        Req::from_url("chapter").query(self)
    }
}

#[derive(Serialize, Debug, Clone, Copy)]
#[serde(rename_all = "lowercase")]
pub enum PurchaseChapterConsumption {
    WaitFree,
    Coin,
}
#[derive(Serialize, Debug, Clone, Copy)]
pub struct PurchaseChapter {
    pub chapter_id: u32,
    pub consumption: PurchaseChapterConsumption,
}
impl ReqRes for PurchaseChapter {
    type Res = ();
    fn to_req(&self) -> Req {
        Req::from_url("purchase_chapter")
            .method(Method::POST)
            .param_body(self)
    }
}
#[derive(Serialize, Debug, Clone, Copy)]
pub struct Favourite {
    pub title_id: u32,
    #[serde(skip)]
    pub is_favourited: bool,
}
impl ReqRes for Favourite {
    type Res = ();
    fn to_req(&self) -> Req {
        Req::from_url("bookmark")
            .method(Method::POST)
            .param_body(self)
    }
}

#[derive(Serialize, Debug, Clone)]
pub struct SendContact {
    mail_address: String,
    body: String,
}
impl ReqRes for SendContact {
    type Res = ();
    fn to_req(&self) -> Req {
        #[derive(Serialize, Debug, Clone)]
        struct SendContactInner<'a> {
            name: &'static str,
            mail_address: &'a str,
            subject: &'static str,
            body: &'a str,
        }
        let body = SendContactInner {
            name: "NAME",
            mail_address: &self.mail_address,
            subject: "SUBJECT",
            body: &self.body,
        };
        Req::from_url("contact")
            .method(Method::POST)
            .param_body(body)
    }
}
#[derive(Serialize, Debug, Clone)]
pub struct Home;
impl ReqRes for Home {
    type Res = HomeResponse;
    fn to_req(&self) -> Req {
        Req::from_url("home")
    }
}

#[derive(Serialize, Debug, Clone)]
pub struct Ranking;
impl ReqRes for Ranking {
    type Res = RankingResponse;
    fn to_req(&self) -> Req {
        Req::from_url("ranking")
    }
}

#[derive(Serialize, Debug, Clone)]
pub struct MyPage;
impl ReqRes for MyPage {
    type Res = MyPageResponse;
    fn to_req(&self) -> Req {
        Req::from_url("mypage")
    }
}

#[derive(Serialize, Debug, Clone)]
pub struct Genres;
impl ReqRes for Genres {
    type Res = SearchResponse;
    fn to_req(&self) -> Req {
        Req::from_url("search_page")
    }
}

#[derive(Serialize, Debug, Clone)]
pub struct Code;
impl ReqRes for Code {
    type Res = CodeResponse;
    fn to_req(&self) -> Req {
        Req::from_url("code")
    }
}

#[derive(Serialize, Debug, Clone)]
pub struct PurchaseHistory;
impl ReqRes for PurchaseHistory {
    type Res = AquisitionHistoryResponse;
    fn to_req(&self) -> Req {
        Req::from_url("items_history")
    }
}

#[derive(Serialize, Debug, Clone)]
pub struct Subscribing;
impl ReqRes for Subscribing {
    type Res = SubscribingProductsResponse;
    fn to_req(&self) -> Req {
        Req::from_url("subscribe_items")
    }
}
