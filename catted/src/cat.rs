// Automatically generated rust module for 'cat.proto' file

#![allow(non_snake_case)]
#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(unused_imports)]
#![allow(unknown_lints)]
#![allow(clippy)]
#![cfg_attr(rustfmt, rustfmt_skip)]


use std::io::Write;
use quick_protobuf::{MessageRead, MessageWrite, BytesReader, Writer, Result};
use std::convert::TryFrom;
use std::ops::Deref;
use std::ops::DerefMut;
use quick_protobuf::sizeofs::*;
use super::*;

#[derive(Debug, Default, PartialEq, Clone)]
pub struct ViewerResponse {
    pub detail: Option<mod_ViewerResponse::Detail>,
    pub error: Option<mod_ViewerResponse::Error>,
}

impl<'a> MessageRead<'a> for ViewerResponse {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.detail = Some(r.read_message::<mod_ViewerResponse::Detail>(bytes)?),
                Ok(18) => msg.error = Some(r.read_message::<mod_ViewerResponse::Error>(bytes)?),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for ViewerResponse {
    fn get_size(&self) -> usize {
        0
        + self.detail.as_ref().map_or(0, |m| 1 + sizeof_len((m).get_size()))
        + self.error.as_ref().map_or(0, |m| 1 + sizeof_len((m).get_size()))
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        if let Some(ref s) = self.detail { w.write_with_tag(10, |w| w.write_message(s))?; }
        if let Some(ref s) = self.error { w.write_with_tag(18, |w| w.write_message(s))?; }
        Ok(())
    }
}

pub mod mod_ViewerResponse {

use super::*;

#[derive(Debug, Default, PartialEq, Clone)]
pub struct Detail {
    pub pages: Vec<mod_ViewerResponse::mod_Detail::Page>,
    pub name: String,
    pub title_id: u32,
    pub is_vertical_scroll_view: bool,
}

impl<'a> MessageRead<'a> for Detail {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.pages.push(r.read_message::<mod_ViewerResponse::mod_Detail::Page>(bytes)?),
                Ok(18) => msg.name = r.read_string(bytes)?.to_owned(),
                Ok(24) => msg.title_id = r.read_uint32(bytes)?,
                Ok(32) => msg.is_vertical_scroll_view = r.read_bool(bytes)?,
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for Detail {
    fn get_size(&self) -> usize {
        0
        + self.pages.iter().map(|s| 1 + sizeof_len((s).get_size())).sum::<usize>()
        + if self.name == String::default() { 0 } else { 1 + sizeof_len((&self.name).len()) }
        + if self.title_id == 0u32 { 0 } else { 1 + sizeof_varint(*(&self.title_id) as u64) }
        + if self.is_vertical_scroll_view == false { 0 } else { 1 + sizeof_varint(*(&self.is_vertical_scroll_view) as u64) }
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        for s in &self.pages { w.write_with_tag(10, |w| w.write_message(s))?; }
        if self.name != String::default() { w.write_with_tag(18, |w| w.write_string(&**&self.name))?; }
        if self.title_id != 0u32 { w.write_with_tag(24, |w| w.write_uint32(*&self.title_id))?; }
        if self.is_vertical_scroll_view != false { w.write_with_tag(32, |w| w.write_bool(*&self.is_vertical_scroll_view))?; }
        Ok(())
    }
}

pub mod mod_Detail {

use super::*;

#[derive(Debug, Default, PartialEq, Clone)]
pub struct Page {
    pub image: Option<mod_ViewerResponse::mod_Detail::mod_Page::Image>,
    pub last: Option<mod_ViewerResponse::mod_Detail::mod_Page::Last>,
}

impl<'a> MessageRead<'a> for Page {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.image = Some(r.read_message::<mod_ViewerResponse::mod_Detail::mod_Page::Image>(bytes)?),
                Ok(18) => msg.last = Some(r.read_message::<mod_ViewerResponse::mod_Detail::mod_Page::Last>(bytes)?),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for Page {
    fn get_size(&self) -> usize {
        0
        + self.image.as_ref().map_or(0, |m| 1 + sizeof_len((m).get_size()))
        + self.last.as_ref().map_or(0, |m| 1 + sizeof_len((m).get_size()))
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        if let Some(ref s) = self.image { w.write_with_tag(10, |w| w.write_message(s))?; }
        if let Some(ref s) = self.last { w.write_with_tag(18, |w| w.write_message(s))?; }
        Ok(())
    }
}

pub mod mod_Page {

use super::*;

#[derive(Debug, Default, PartialEq, Clone)]
pub struct Image {
    pub url: String,
    pub description_key: String,
}

impl<'a> MessageRead<'a> for Image {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.url = r.read_string(bytes)?.to_owned(),
                Ok(18) => msg.description_key = r.read_string(bytes)?.to_owned(),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for Image {
    fn get_size(&self) -> usize {
        0
        + if self.url == String::default() { 0 } else { 1 + sizeof_len((&self.url).len()) }
        + if self.description_key == String::default() { 0 } else { 1 + sizeof_len((&self.description_key).len()) }
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        if self.url != String::default() { w.write_with_tag(10, |w| w.write_string(&**&self.url))?; }
        if self.description_key != String::default() { w.write_with_tag(18, |w| w.write_string(&**&self.description_key))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct Last {
    pub next: Option<ChapterSummary>,
    pub recommended_titles: Vec<TitleSummary>,
    pub is_bookmarked: bool,
    pub ticked_info: Option<TicketInfo>,
    pub currency: Option<Currency>,
}

impl<'a> MessageRead<'a> for Last {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.next = Some(r.read_message::<ChapterSummary>(bytes)?),
                Ok(18) => msg.recommended_titles.push(r.read_message::<TitleSummary>(bytes)?),
                Ok(808) => msg.is_bookmarked = r.read_bool(bytes)?,
                Ok(818) => msg.ticked_info = Some(r.read_message::<TicketInfo>(bytes)?),
                Ok(826) => msg.currency = Some(r.read_message::<Currency>(bytes)?),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for Last {
    fn get_size(&self) -> usize {
        0
        + self.next.as_ref().map_or(0, |m| 1 + sizeof_len((m).get_size()))
        + self.recommended_titles.iter().map(|s| 1 + sizeof_len((s).get_size())).sum::<usize>()
        + if self.is_bookmarked == false { 0 } else { 2 + sizeof_varint(*(&self.is_bookmarked) as u64) }
        + self.ticked_info.as_ref().map_or(0, |m| 2 + sizeof_len((m).get_size()))
        + self.currency.as_ref().map_or(0, |m| 2 + sizeof_len((m).get_size()))
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        if let Some(ref s) = self.next { w.write_with_tag(10, |w| w.write_message(s))?; }
        for s in &self.recommended_titles { w.write_with_tag(18, |w| w.write_message(s))?; }
        if self.is_bookmarked != false { w.write_with_tag(808, |w| w.write_bool(*&self.is_bookmarked))?; }
        if let Some(ref s) = self.ticked_info { w.write_with_tag(818, |w| w.write_message(s))?; }
        if let Some(ref s) = self.currency { w.write_with_tag(826, |w| w.write_message(s))?; }
        Ok(())
    }
}

}

}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct Error {
    pub info: mod_ViewerResponse::mod_Error::Info,
    pub summary: Option<ChapterSummary>,
}

impl<'a> MessageRead<'a> for Error {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(8) => msg.info = r.read_enum(bytes)?,
                Ok(18) => msg.summary = Some(r.read_message::<ChapterSummary>(bytes)?),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for Error {
    fn get_size(&self) -> usize {
        0
        + if self.info == cat::mod_ViewerResponse::mod_Error::Info::LOGGIN_NEEDED { 0 } else { 1 + sizeof_varint(*(&self.info) as u64) }
        + self.summary.as_ref().map_or(0, |m| 1 + sizeof_len((m).get_size()))
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        if self.info != cat::mod_ViewerResponse::mod_Error::Info::LOGGIN_NEEDED { w.write_with_tag(8, |w| w.write_enum(*&self.info as i32))?; }
        if let Some(ref s) = self.summary { w.write_with_tag(18, |w| w.write_message(s))?; }
        Ok(())
    }
}

pub mod mod_Error {


#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum Info {
    LOGGIN_NEEDED = 0,
    NOT_PURCHASED = 1,
}

impl Default for Info {
    fn default() -> Self {
        Info::LOGGIN_NEEDED
    }
}

impl From<i32> for Info {
    fn from(i: i32) -> Self {
        match i {
            0 => Info::LOGGIN_NEEDED,
            1 => Info::NOT_PURCHASED,
            _ => Self::default(),
        }
    }
}

impl<'a> From<&'a str> for Info {
    fn from(s: &'a str) -> Self {
        match s {
            "LOGGIN_NEEDED" => Info::LOGGIN_NEEDED,
            "NOT_PURCHASED" => Info::NOT_PURCHASED,
            _ => Self::default(),
        }
    }
}

}

}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct ChapterSummary {
    pub id: u32,
    pub thumbnail_url: String,
    pub name: String,
    pub publish_date: String,
    pub consumption: mod_ChapterSummary::Consumption,
    pub price: u32,
    pub is_recently_updated: bool,
    pub purchase_date: u32,
    pub is_read: bool,
}

impl<'a> MessageRead<'a> for ChapterSummary {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(8) => msg.id = r.read_uint32(bytes)?,
                Ok(18) => msg.thumbnail_url = r.read_string(bytes)?.to_owned(),
                Ok(26) => msg.name = r.read_string(bytes)?.to_owned(),
                Ok(34) => msg.publish_date = r.read_string(bytes)?.to_owned(),
                Ok(40) => msg.consumption = r.read_enum(bytes)?,
                Ok(48) => msg.price = r.read_uint32(bytes)?,
                Ok(816) => msg.is_recently_updated = r.read_bool(bytes)?,
                Ok(1608) => msg.purchase_date = r.read_uint32(bytes)?,
                Ok(1616) => msg.is_read = r.read_bool(bytes)?,
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for ChapterSummary {
    fn get_size(&self) -> usize {
        0
        + if self.id == 0u32 { 0 } else { 1 + sizeof_varint(*(&self.id) as u64) }
        + if self.thumbnail_url == String::default() { 0 } else { 1 + sizeof_len((&self.thumbnail_url).len()) }
        + if self.name == String::default() { 0 } else { 1 + sizeof_len((&self.name).len()) }
        + if self.publish_date == String::default() { 0 } else { 1 + sizeof_len((&self.publish_date).len()) }
        + if self.consumption == cat::mod_ChapterSummary::Consumption::FREE { 0 } else { 1 + sizeof_varint(*(&self.consumption) as u64) }
        + if self.price == 0u32 { 0 } else { 1 + sizeof_varint(*(&self.price) as u64) }
        + if self.is_recently_updated == false { 0 } else { 2 + sizeof_varint(*(&self.is_recently_updated) as u64) }
        + if self.purchase_date == 0u32 { 0 } else { 2 + sizeof_varint(*(&self.purchase_date) as u64) }
        + if self.is_read == false { 0 } else { 2 + sizeof_varint(*(&self.is_read) as u64) }
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        if self.id != 0u32 { w.write_with_tag(8, |w| w.write_uint32(*&self.id))?; }
        if self.thumbnail_url != String::default() { w.write_with_tag(18, |w| w.write_string(&**&self.thumbnail_url))?; }
        if self.name != String::default() { w.write_with_tag(26, |w| w.write_string(&**&self.name))?; }
        if self.publish_date != String::default() { w.write_with_tag(34, |w| w.write_string(&**&self.publish_date))?; }
        if self.consumption != cat::mod_ChapterSummary::Consumption::FREE { w.write_with_tag(40, |w| w.write_enum(*&self.consumption as i32))?; }
        if self.price != 0u32 { w.write_with_tag(48, |w| w.write_uint32(*&self.price))?; }
        if self.is_recently_updated != false { w.write_with_tag(816, |w| w.write_bool(*&self.is_recently_updated))?; }
        if self.purchase_date != 0u32 { w.write_with_tag(1608, |w| w.write_uint32(*&self.purchase_date))?; }
        if self.is_read != false { w.write_with_tag(1616, |w| w.write_bool(*&self.is_read))?; }
        Ok(())
    }
}

pub mod mod_ChapterSummary {


#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum Consumption {
    FREE = 0,
    TICKET = 1,
    COIN = 2,
    ADVANCE = 3,
}

impl Default for Consumption {
    fn default() -> Self {
        Consumption::FREE
    }
}

impl From<i32> for Consumption {
    fn from(i: i32) -> Self {
        match i {
            0 => Consumption::FREE,
            1 => Consumption::TICKET,
            2 => Consumption::COIN,
            3 => Consumption::ADVANCE,
            _ => Self::default(),
        }
    }
}

impl<'a> From<&'a str> for Consumption {
    fn from(s: &'a str) -> Self {
        match s {
            "FREE" => Consumption::FREE,
            "TICKET" => Consumption::TICKET,
            "COIN" => Consumption::COIN,
            "ADVANCE" => Consumption::ADVANCE,
            _ => Self::default(),
        }
    }
}

}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct ChapterRequest {
    pub id: u32,
}

impl<'a> MessageRead<'a> for ChapterRequest {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(8) => msg.id = r.read_uint32(bytes)?,
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for ChapterRequest {
    fn get_size(&self) -> usize {
        0
        + if self.id == 0u32 { 0 } else { 1 + sizeof_varint(*(&self.id) as u64) }
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        if self.id != 0u32 { w.write_with_tag(8, |w| w.write_uint32(*&self.id))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct Currency {
    pub bonus_coins: i32,
    pub paid_coins: i32,
}

impl<'a> MessageRead<'a> for Currency {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(8) => msg.bonus_coins = r.read_int32(bytes)?,
                Ok(16) => msg.paid_coins = r.read_int32(bytes)?,
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for Currency {
    fn get_size(&self) -> usize {
        0
        + if self.bonus_coins == 0i32 { 0 } else { 1 + sizeof_varint(*(&self.bonus_coins) as u64) }
        + if self.paid_coins == 0i32 { 0 } else { 1 + sizeof_varint(*(&self.paid_coins) as u64) }
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        if self.bonus_coins != 0i32 { w.write_with_tag(8, |w| w.write_int32(*&self.bonus_coins))?; }
        if self.paid_coins != 0i32 { w.write_with_tag(16, |w| w.write_int32(*&self.paid_coins))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct MoveTo {
    pub title: u32,
    pub titles_with_tag: Option<Tag>,
    pub url: String,
    pub section_id: u32,
}

impl<'a> MessageRead<'a> for MoveTo {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(8) => msg.title = r.read_uint32(bytes)?,
                Ok(18) => msg.titles_with_tag = Some(r.read_message::<Tag>(bytes)?),
                Ok(26) => msg.url = r.read_string(bytes)?.to_owned(),
                Ok(32) => msg.section_id = r.read_uint32(bytes)?,
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for MoveTo {
    fn get_size(&self) -> usize {
        0
        + if self.title == 0u32 { 0 } else { 1 + sizeof_varint(*(&self.title) as u64) }
        + self.titles_with_tag.as_ref().map_or(0, |m| 1 + sizeof_len((m).get_size()))
        + if self.url == String::default() { 0 } else { 1 + sizeof_len((&self.url).len()) }
        + if self.section_id == 0u32 { 0 } else { 1 + sizeof_varint(*(&self.section_id) as u64) }
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        if self.title != 0u32 { w.write_with_tag(8, |w| w.write_uint32(*&self.title))?; }
        if let Some(ref s) = self.titles_with_tag { w.write_with_tag(18, |w| w.write_message(s))?; }
        if self.url != String::default() { w.write_with_tag(26, |w| w.write_string(&**&self.url))?; }
        if self.section_id != 0u32 { w.write_with_tag(32, |w| w.write_uint32(*&self.section_id))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct Tag {
    pub id: u32,
    pub name: String,
}

impl<'a> MessageRead<'a> for Tag {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(8) => msg.id = r.read_uint32(bytes)?,
                Ok(18) => msg.name = r.read_string(bytes)?.to_owned(),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for Tag {
    fn get_size(&self) -> usize {
        0
        + if self.id == 0u32 { 0 } else { 1 + sizeof_varint(*(&self.id) as u64) }
        + if self.name == String::default() { 0 } else { 1 + sizeof_len((&self.name).len()) }
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        if self.id != 0u32 { w.write_with_tag(8, |w| w.write_uint32(*&self.id))?; }
        if self.name != String::default() { w.write_with_tag(18, |w| w.write_string(&**&self.name))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct TicketInfo {
    pub next_recovery_date: String,
}

impl<'a> MessageRead<'a> for TicketInfo {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.next_recovery_date = r.read_string(bytes)?.to_owned(),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for TicketInfo {
    fn get_size(&self) -> usize {
        0
        + if self.next_recovery_date == String::default() { 0 } else { 1 + sizeof_len((&self.next_recovery_date).len()) }
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        if self.next_recovery_date != String::default() { w.write_with_tag(10, |w| w.write_string(&**&self.next_recovery_date))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct HomeResponse {
    pub banners: Vec<mod_HomeResponse::Banner>,
    pub sections: Vec<mod_HomeResponse::Section>,
    pub notification: Option<mod_HomeResponse::mod_Section::Notification>,
}

impl<'a> MessageRead<'a> for HomeResponse {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.banners.push(r.read_message::<mod_HomeResponse::Banner>(bytes)?),
                Ok(18) => msg.sections.push(r.read_message::<mod_HomeResponse::Section>(bytes)?),
                Ok(810) => msg.notification = Some(r.read_message::<mod_HomeResponse::mod_Section::Notification>(bytes)?),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for HomeResponse {
    fn get_size(&self) -> usize {
        0
        + self.banners.iter().map(|s| 1 + sizeof_len((s).get_size())).sum::<usize>()
        + self.sections.iter().map(|s| 1 + sizeof_len((s).get_size())).sum::<usize>()
        + self.notification.as_ref().map_or(0, |m| 2 + sizeof_len((m).get_size()))
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        for s in &self.banners { w.write_with_tag(10, |w| w.write_message(s))?; }
        for s in &self.sections { w.write_with_tag(18, |w| w.write_message(s))?; }
        if let Some(ref s) = self.notification { w.write_with_tag(810, |w| w.write_message(s))?; }
        Ok(())
    }
}

pub mod mod_HomeResponse {

use super::*;

#[derive(Debug, Default, PartialEq, Clone)]
pub struct Banner {
    pub image_url: String,
    pub name: String,
    pub move_to: Option<MoveTo>,
}

impl<'a> MessageRead<'a> for Banner {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.image_url = r.read_string(bytes)?.to_owned(),
                Ok(18) => msg.name = r.read_string(bytes)?.to_owned(),
                Ok(26) => msg.move_to = Some(r.read_message::<MoveTo>(bytes)?),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for Banner {
    fn get_size(&self) -> usize {
        0
        + if self.image_url == String::default() { 0 } else { 1 + sizeof_len((&self.image_url).len()) }
        + if self.name == String::default() { 0 } else { 1 + sizeof_len((&self.name).len()) }
        + self.move_to.as_ref().map_or(0, |m| 1 + sizeof_len((m).get_size()))
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        if self.image_url != String::default() { w.write_with_tag(10, |w| w.write_string(&**&self.image_url))?; }
        if self.name != String::default() { w.write_with_tag(18, |w| w.write_string(&**&self.name))?; }
        if let Some(ref s) = self.move_to { w.write_with_tag(26, |w| w.write_message(s))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct Section {
    pub header: String,
    pub titles: Option<mod_HomeResponse::mod_Section::Titles>,
    pub ranking: Option<mod_HomeResponse::mod_Section::Ranking>,
    pub single_banner: Option<mod_HomeResponse::Banner>,
    pub login_induction: Option<mod_HomeResponse::mod_Section::LoginInduction>,
    pub more_link: Option<MoveTo>,
    pub bg_colour: String,
    pub subject_and_name_text_colour: String,
    pub description_text_colour: String,
}

impl<'a> MessageRead<'a> for Section {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.header = r.read_string(bytes)?.to_owned(),
                Ok(26) => msg.titles = Some(r.read_message::<mod_HomeResponse::mod_Section::Titles>(bytes)?),
                Ok(34) => msg.ranking = Some(r.read_message::<mod_HomeResponse::mod_Section::Ranking>(bytes)?),
                Ok(42) => msg.single_banner = Some(r.read_message::<mod_HomeResponse::Banner>(bytes)?),
                Ok(82) => msg.login_induction = Some(r.read_message::<mod_HomeResponse::mod_Section::LoginInduction>(bytes)?),
                Ok(50) => msg.more_link = Some(r.read_message::<MoveTo>(bytes)?),
                Ok(58) => msg.bg_colour = r.read_string(bytes)?.to_owned(),
                Ok(66) => msg.subject_and_name_text_colour = r.read_string(bytes)?.to_owned(),
                Ok(74) => msg.description_text_colour = r.read_string(bytes)?.to_owned(),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for Section {
    fn get_size(&self) -> usize {
        0
        + if self.header == String::default() { 0 } else { 1 + sizeof_len((&self.header).len()) }
        + self.titles.as_ref().map_or(0, |m| 1 + sizeof_len((m).get_size()))
        + self.ranking.as_ref().map_or(0, |m| 1 + sizeof_len((m).get_size()))
        + self.single_banner.as_ref().map_or(0, |m| 1 + sizeof_len((m).get_size()))
        + self.login_induction.as_ref().map_or(0, |m| 1 + sizeof_len((m).get_size()))
        + self.more_link.as_ref().map_or(0, |m| 1 + sizeof_len((m).get_size()))
        + if self.bg_colour == String::default() { 0 } else { 1 + sizeof_len((&self.bg_colour).len()) }
        + if self.subject_and_name_text_colour == String::default() { 0 } else { 1 + sizeof_len((&self.subject_and_name_text_colour).len()) }
        + if self.description_text_colour == String::default() { 0 } else { 1 + sizeof_len((&self.description_text_colour).len()) }
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        if self.header != String::default() { w.write_with_tag(10, |w| w.write_string(&**&self.header))?; }
        if let Some(ref s) = self.titles { w.write_with_tag(26, |w| w.write_message(s))?; }
        if let Some(ref s) = self.ranking { w.write_with_tag(34, |w| w.write_message(s))?; }
        if let Some(ref s) = self.single_banner { w.write_with_tag(42, |w| w.write_message(s))?; }
        if let Some(ref s) = self.login_induction { w.write_with_tag(82, |w| w.write_message(s))?; }
        if let Some(ref s) = self.more_link { w.write_with_tag(50, |w| w.write_message(s))?; }
        if self.bg_colour != String::default() { w.write_with_tag(58, |w| w.write_string(&**&self.bg_colour))?; }
        if self.subject_and_name_text_colour != String::default() { w.write_with_tag(66, |w| w.write_string(&**&self.subject_and_name_text_colour))?; }
        if self.description_text_colour != String::default() { w.write_with_tag(74, |w| w.write_string(&**&self.description_text_colour))?; }
        Ok(())
    }
}

pub mod mod_Section {

use super::*;

#[derive(Debug, Default, PartialEq, Clone)]
pub struct Ranking {
    pub categories: Vec<mod_HomeResponse::mod_Section::mod_Ranking::Category>,
}

impl<'a> MessageRead<'a> for Ranking {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.categories.push(r.read_message::<mod_HomeResponse::mod_Section::mod_Ranking::Category>(bytes)?),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for Ranking {
    fn get_size(&self) -> usize {
        0
        + self.categories.iter().map(|s| 1 + sizeof_len((s).get_size())).sum::<usize>()
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        for s in &self.categories { w.write_with_tag(10, |w| w.write_message(s))?; }
        Ok(())
    }
}

pub mod mod_Ranking {

use super::*;

#[derive(Debug, Default, PartialEq, Clone)]
pub struct Category {
    pub name: String,
    pub titles: Vec<TitleSummary>,
}

impl<'a> MessageRead<'a> for Category {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.name = r.read_string(bytes)?.to_owned(),
                Ok(18) => msg.titles.push(r.read_message::<TitleSummary>(bytes)?),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for Category {
    fn get_size(&self) -> usize {
        0
        + if self.name == String::default() { 0 } else { 1 + sizeof_len((&self.name).len()) }
        + self.titles.iter().map(|s| 1 + sizeof_len((s).get_size())).sum::<usize>()
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        if self.name != String::default() { w.write_with_tag(10, |w| w.write_string(&**&self.name))?; }
        for s in &self.titles { w.write_with_tag(18, |w| w.write_message(s))?; }
        Ok(())
    }
}

}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct LoginInduction {
    pub image_url: String,
}

impl<'a> MessageRead<'a> for LoginInduction {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.image_url = r.read_string(bytes)?.to_owned(),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for LoginInduction {
    fn get_size(&self) -> usize {
        0
        + if self.image_url == String::default() { 0 } else { 1 + sizeof_len((&self.image_url).len()) }
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        if self.image_url != String::default() { w.write_with_tag(10, |w| w.write_string(&**&self.image_url))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct Titles {
    pub titles: Vec<TitleSummary>,
    pub layout: mod_HomeResponse::mod_Section::mod_Titles::Layout,
}

impl<'a> MessageRead<'a> for Titles {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.titles.push(r.read_message::<TitleSummary>(bytes)?),
                Ok(16) => msg.layout = r.read_enum(bytes)?,
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for Titles {
    fn get_size(&self) -> usize {
        0
        + self.titles.iter().map(|s| 1 + sizeof_len((s).get_size())).sum::<usize>()
        + if self.layout == cat::mod_HomeResponse::mod_Section::mod_Titles::Layout::SMALL { 0 } else { 1 + sizeof_varint(*(&self.layout) as u64) }
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        for s in &self.titles { w.write_with_tag(10, |w| w.write_message(s))?; }
        if self.layout != cat::mod_HomeResponse::mod_Section::mod_Titles::Layout::SMALL { w.write_with_tag(16, |w| w.write_enum(*&self.layout as i32))?; }
        Ok(())
    }
}

pub mod mod_Titles {


#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum Layout {
    SMALL = 0,
    LARGE = 1,
    SMALL_WITH_TICKET_INFO = 2,
}

impl Default for Layout {
    fn default() -> Self {
        Layout::SMALL
    }
}

impl From<i32> for Layout {
    fn from(i: i32) -> Self {
        match i {
            0 => Layout::SMALL,
            1 => Layout::LARGE,
            2 => Layout::SMALL_WITH_TICKET_INFO,
            _ => Self::default(),
        }
    }
}

impl<'a> From<&'a str> for Layout {
    fn from(s: &'a str) -> Self {
        match s {
            "SMALL" => Layout::SMALL,
            "LARGE" => Layout::LARGE,
            "SMALL_WITH_TICKET_INFO" => Layout::SMALL_WITH_TICKET_INFO,
            _ => Self::default(),
        }
    }
}

}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct Notification {
    pub simple_message: Option<mod_HomeResponse::mod_Section::mod_Notification::SimpleMessage>,
    pub reward_ad: Option<mod_HomeResponse::mod_Section::mod_Notification::RewardAd>,
}

impl<'a> MessageRead<'a> for Notification {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.simple_message = Some(r.read_message::<mod_HomeResponse::mod_Section::mod_Notification::SimpleMessage>(bytes)?),
                Ok(18) => msg.reward_ad = Some(r.read_message::<mod_HomeResponse::mod_Section::mod_Notification::RewardAd>(bytes)?),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for Notification {
    fn get_size(&self) -> usize {
        0
        + self.simple_message.as_ref().map_or(0, |m| 1 + sizeof_len((m).get_size()))
        + self.reward_ad.as_ref().map_or(0, |m| 1 + sizeof_len((m).get_size()))
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        if let Some(ref s) = self.simple_message { w.write_with_tag(10, |w| w.write_message(s))?; }
        if let Some(ref s) = self.reward_ad { w.write_with_tag(18, |w| w.write_message(s))?; }
        Ok(())
    }
}

pub mod mod_Notification {

use super::*;

#[derive(Debug, Default, PartialEq, Clone)]
pub struct RewardAd {
    pub reward_amount: u32,
    pub url: String,
}

impl<'a> MessageRead<'a> for RewardAd {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(8) => msg.reward_amount = r.read_uint32(bytes)?,
                Ok(18) => msg.url = r.read_string(bytes)?.to_owned(),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for RewardAd {
    fn get_size(&self) -> usize {
        0
        + if self.reward_amount == 0u32 { 0 } else { 1 + sizeof_varint(*(&self.reward_amount) as u64) }
        + if self.url == String::default() { 0 } else { 1 + sizeof_len((&self.url).len()) }
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        if self.reward_amount != 0u32 { w.write_with_tag(8, |w| w.write_uint32(*&self.reward_amount))?; }
        if self.url != String::default() { w.write_with_tag(18, |w| w.write_string(&**&self.url))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct SimpleMessage {
    pub subject: String,
    pub body: String,
}

impl<'a> MessageRead<'a> for SimpleMessage {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.subject = r.read_string(bytes)?.to_owned(),
                Ok(18) => msg.body = r.read_string(bytes)?.to_owned(),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for SimpleMessage {
    fn get_size(&self) -> usize {
        0
        + if self.subject == String::default() { 0 } else { 1 + sizeof_len((&self.subject).len()) }
        + if self.body == String::default() { 0 } else { 1 + sizeof_len((&self.body).len()) }
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        if self.subject != String::default() { w.write_with_tag(10, |w| w.write_string(&**&self.subject))?; }
        if self.body != String::default() { w.write_with_tag(18, |w| w.write_string(&**&self.body))?; }
        Ok(())
    }
}

}

}

}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct CodeResponse {
    pub code: String,
}

impl<'a> MessageRead<'a> for CodeResponse {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.code = r.read_string(bytes)?.to_owned(),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for CodeResponse {
    fn get_size(&self) -> usize {
        0
        + if self.code == String::default() { 0 } else { 1 + sizeof_len((&self.code).len()) }
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        if self.code != String::default() { w.write_with_tag(10, |w| w.write_string(&**&self.code))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct MyPageResponse {
    pub currency: Option<Currency>,
    pub bookmarked_titles: Vec<TitleSummary>,
    pub history_titles: Vec<TitleSummary>,
    pub reward_url: String,
}

impl<'a> MessageRead<'a> for MyPageResponse {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.currency = Some(r.read_message::<Currency>(bytes)?),
                Ok(18) => msg.bookmarked_titles.push(r.read_message::<TitleSummary>(bytes)?),
                Ok(26) => msg.history_titles.push(r.read_message::<TitleSummary>(bytes)?),
                Ok(34) => msg.reward_url = r.read_string(bytes)?.to_owned(),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for MyPageResponse {
    fn get_size(&self) -> usize {
        0
        + self.currency.as_ref().map_or(0, |m| 1 + sizeof_len((m).get_size()))
        + self.bookmarked_titles.iter().map(|s| 1 + sizeof_len((s).get_size())).sum::<usize>()
        + self.history_titles.iter().map(|s| 1 + sizeof_len((s).get_size())).sum::<usize>()
        + if self.reward_url == String::default() { 0 } else { 1 + sizeof_len((&self.reward_url).len()) }
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        if let Some(ref s) = self.currency { w.write_with_tag(10, |w| w.write_message(s))?; }
        for s in &self.bookmarked_titles { w.write_with_tag(18, |w| w.write_message(s))?; }
        for s in &self.history_titles { w.write_with_tag(26, |w| w.write_message(s))?; }
        if self.reward_url != String::default() { w.write_with_tag(34, |w| w.write_string(&**&self.reward_url))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct SetFavourite {
    pub title_id: u32,
}

impl<'a> MessageRead<'a> for SetFavourite {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(8) => msg.title_id = r.read_uint32(bytes)?,
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for SetFavourite {
    fn get_size(&self) -> usize {
        0
        + if self.title_id == 0u32 { 0 } else { 1 + sizeof_varint(*(&self.title_id) as u64) }
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        if self.title_id != 0u32 { w.write_with_tag(8, |w| w.write_uint32(*&self.title_id))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct AquisitionHistoryResponse {
    pub entries: Vec<mod_AquisitionHistoryResponse::Entry>,
}

impl<'a> MessageRead<'a> for AquisitionHistoryResponse {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.entries.push(r.read_message::<mod_AquisitionHistoryResponse::Entry>(bytes)?),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for AquisitionHistoryResponse {
    fn get_size(&self) -> usize {
        0
        + self.entries.iter().map(|s| 1 + sizeof_len((s).get_size())).sum::<usize>()
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        for s in &self.entries { w.write_with_tag(10, |w| w.write_message(s))?; }
        Ok(())
    }
}

pub mod mod_AquisitionHistoryResponse {

use super::*;

#[derive(Debug, Default, PartialEq, Clone)]
pub struct Entry {
    pub subject: String,
    pub date: String,
    pub paid_coins_amount: u32,
    pub bonus_coins_amount: u32,
}

impl<'a> MessageRead<'a> for Entry {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.subject = r.read_string(bytes)?.to_owned(),
                Ok(18) => msg.date = r.read_string(bytes)?.to_owned(),
                Ok(24) => msg.paid_coins_amount = r.read_uint32(bytes)?,
                Ok(32) => msg.bonus_coins_amount = r.read_uint32(bytes)?,
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for Entry {
    fn get_size(&self) -> usize {
        0
        + if self.subject == String::default() { 0 } else { 1 + sizeof_len((&self.subject).len()) }
        + if self.date == String::default() { 0 } else { 1 + sizeof_len((&self.date).len()) }
        + if self.paid_coins_amount == 0u32 { 0 } else { 1 + sizeof_varint(*(&self.paid_coins_amount) as u64) }
        + if self.bonus_coins_amount == 0u32 { 0 } else { 1 + sizeof_varint(*(&self.bonus_coins_amount) as u64) }
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        if self.subject != String::default() { w.write_with_tag(10, |w| w.write_string(&**&self.subject))?; }
        if self.date != String::default() { w.write_with_tag(18, |w| w.write_string(&**&self.date))?; }
        if self.paid_coins_amount != 0u32 { w.write_with_tag(24, |w| w.write_uint32(*&self.paid_coins_amount))?; }
        if self.bonus_coins_amount != 0u32 { w.write_with_tag(32, |w| w.write_uint32(*&self.bonus_coins_amount))?; }
        Ok(())
    }
}

}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct PurchaseProductsResponse {
    pub currency: Option<Currency>,
    pub products: Vec<mod_PurchaseProductsResponse::Product>,
}

impl<'a> MessageRead<'a> for PurchaseProductsResponse {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.currency = Some(r.read_message::<Currency>(bytes)?),
                Ok(18) => msg.products.push(r.read_message::<mod_PurchaseProductsResponse::Product>(bytes)?),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for PurchaseProductsResponse {
    fn get_size(&self) -> usize {
        0
        + self.currency.as_ref().map_or(0, |m| 1 + sizeof_len((m).get_size()))
        + self.products.iter().map(|s| 1 + sizeof_len((s).get_size())).sum::<usize>()
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        if let Some(ref s) = self.currency { w.write_with_tag(10, |w| w.write_message(s))?; }
        for s in &self.products { w.write_with_tag(18, |w| w.write_message(s))?; }
        Ok(())
    }
}

pub mod mod_PurchaseProductsResponse {

use super::*;

#[derive(Debug, Default, PartialEq, Clone)]
pub struct Product {
    pub id: u32,
    pub amount: u32,
    pub bonus_amount: u32,
    pub price: u32,
    pub url: String,
}

impl<'a> MessageRead<'a> for Product {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(8) => msg.id = r.read_uint32(bytes)?,
                Ok(16) => msg.amount = r.read_uint32(bytes)?,
                Ok(24) => msg.bonus_amount = r.read_uint32(bytes)?,
                Ok(32) => msg.price = r.read_uint32(bytes)?,
                Ok(42) => msg.url = r.read_string(bytes)?.to_owned(),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for Product {
    fn get_size(&self) -> usize {
        0
        + if self.id == 0u32 { 0 } else { 1 + sizeof_varint(*(&self.id) as u64) }
        + if self.amount == 0u32 { 0 } else { 1 + sizeof_varint(*(&self.amount) as u64) }
        + if self.bonus_amount == 0u32 { 0 } else { 1 + sizeof_varint(*(&self.bonus_amount) as u64) }
        + if self.price == 0u32 { 0 } else { 1 + sizeof_varint(*(&self.price) as u64) }
        + if self.url == String::default() { 0 } else { 1 + sizeof_len((&self.url).len()) }
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        if self.id != 0u32 { w.write_with_tag(8, |w| w.write_uint32(*&self.id))?; }
        if self.amount != 0u32 { w.write_with_tag(16, |w| w.write_uint32(*&self.amount))?; }
        if self.bonus_amount != 0u32 { w.write_with_tag(24, |w| w.write_uint32(*&self.bonus_amount))?; }
        if self.price != 0u32 { w.write_with_tag(32, |w| w.write_uint32(*&self.price))?; }
        if self.url != String::default() { w.write_with_tag(42, |w| w.write_string(&**&self.url))?; }
        Ok(())
    }
}

}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct PurchaseChapterRequest {
    pub chapter_id: u32,
    pub consumption: mod_PurchaseChapterRequest::Consumption,
}

impl<'a> MessageRead<'a> for PurchaseChapterRequest {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(8) => msg.chapter_id = r.read_uint32(bytes)?,
                Ok(16) => msg.consumption = r.read_enum(bytes)?,
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for PurchaseChapterRequest {
    fn get_size(&self) -> usize {
        0
        + if self.chapter_id == 0u32 { 0 } else { 1 + sizeof_varint(*(&self.chapter_id) as u64) }
        + if self.consumption == cat::mod_PurchaseChapterRequest::Consumption::WAITFREE { 0 } else { 1 + sizeof_varint(*(&self.consumption) as u64) }
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        if self.chapter_id != 0u32 { w.write_with_tag(8, |w| w.write_uint32(*&self.chapter_id))?; }
        if self.consumption != cat::mod_PurchaseChapterRequest::Consumption::WAITFREE { w.write_with_tag(16, |w| w.write_enum(*&self.consumption as i32))?; }
        Ok(())
    }
}

pub mod mod_PurchaseChapterRequest {


#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum Consumption {
    WAITFREE = 0,
    COIN = 1,
}

impl Default for Consumption {
    fn default() -> Self {
        Consumption::WAITFREE
    }
}

impl From<i32> for Consumption {
    fn from(i: i32) -> Self {
        match i {
            0 => Consumption::WAITFREE,
            1 => Consumption::COIN,
            _ => Self::default(),
        }
    }
}

impl<'a> From<&'a str> for Consumption {
    fn from(s: &'a str) -> Self {
        match s {
            "WAITFREE" => Consumption::WAITFREE,
            "COIN" => Consumption::COIN,
            _ => Self::default(),
        }
    }
}

}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct SubscribeProduct {
    pub id: u32,
    pub amount: u32,
    pub price_expression: String,
    pub bonus_amount: u32,
    pub button_title: String,
    pub url: String,
    pub is_subscribed: bool,
    pub is_resubscription_suspended: bool,
}

impl<'a> MessageRead<'a> for SubscribeProduct {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(8) => msg.id = r.read_uint32(bytes)?,
                Ok(16) => msg.amount = r.read_uint32(bytes)?,
                Ok(26) => msg.price_expression = r.read_string(bytes)?.to_owned(),
                Ok(32) => msg.bonus_amount = r.read_uint32(bytes)?,
                Ok(42) => msg.button_title = r.read_string(bytes)?.to_owned(),
                Ok(50) => msg.url = r.read_string(bytes)?.to_owned(),
                Ok(56) => msg.is_subscribed = r.read_bool(bytes)?,
                Ok(64) => msg.is_resubscription_suspended = r.read_bool(bytes)?,
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for SubscribeProduct {
    fn get_size(&self) -> usize {
        0
        + if self.id == 0u32 { 0 } else { 1 + sizeof_varint(*(&self.id) as u64) }
        + if self.amount == 0u32 { 0 } else { 1 + sizeof_varint(*(&self.amount) as u64) }
        + if self.price_expression == String::default() { 0 } else { 1 + sizeof_len((&self.price_expression).len()) }
        + if self.bonus_amount == 0u32 { 0 } else { 1 + sizeof_varint(*(&self.bonus_amount) as u64) }
        + if self.button_title == String::default() { 0 } else { 1 + sizeof_len((&self.button_title).len()) }
        + if self.url == String::default() { 0 } else { 1 + sizeof_len((&self.url).len()) }
        + if self.is_subscribed == false { 0 } else { 1 + sizeof_varint(*(&self.is_subscribed) as u64) }
        + if self.is_resubscription_suspended == false { 0 } else { 1 + sizeof_varint(*(&self.is_resubscription_suspended) as u64) }
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        if self.id != 0u32 { w.write_with_tag(8, |w| w.write_uint32(*&self.id))?; }
        if self.amount != 0u32 { w.write_with_tag(16, |w| w.write_uint32(*&self.amount))?; }
        if self.price_expression != String::default() { w.write_with_tag(26, |w| w.write_string(&**&self.price_expression))?; }
        if self.bonus_amount != 0u32 { w.write_with_tag(32, |w| w.write_uint32(*&self.bonus_amount))?; }
        if self.button_title != String::default() { w.write_with_tag(42, |w| w.write_string(&**&self.button_title))?; }
        if self.url != String::default() { w.write_with_tag(50, |w| w.write_string(&**&self.url))?; }
        if self.is_subscribed != false { w.write_with_tag(56, |w| w.write_bool(*&self.is_subscribed))?; }
        if self.is_resubscription_suspended != false { w.write_with_tag(64, |w| w.write_bool(*&self.is_resubscription_suspended))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct SubscribingProductsResponse {
    pub products: Vec<SubscribeProduct>,
}

impl<'a> MessageRead<'a> for SubscribingProductsResponse {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.products.push(r.read_message::<SubscribeProduct>(bytes)?),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for SubscribingProductsResponse {
    fn get_size(&self) -> usize {
        0
        + self.products.iter().map(|s| 1 + sizeof_len((s).get_size())).sum::<usize>()
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        for s in &self.products { w.write_with_tag(10, |w| w.write_message(s))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct RankingResponse {
    pub genres: Vec<mod_RankingResponse::Genre>,
}

impl<'a> MessageRead<'a> for RankingResponse {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.genres.push(r.read_message::<mod_RankingResponse::Genre>(bytes)?),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for RankingResponse {
    fn get_size(&self) -> usize {
        0
        + self.genres.iter().map(|s| 1 + sizeof_len((s).get_size())).sum::<usize>()
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        for s in &self.genres { w.write_with_tag(10, |w| w.write_message(s))?; }
        Ok(())
    }
}

pub mod mod_RankingResponse {

use super::*;

#[derive(Debug, Default, PartialEq, Clone)]
pub struct Genre {
    pub name: String,
    pub titles: Vec<TitleSummary>,
}

impl<'a> MessageRead<'a> for Genre {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.name = r.read_string(bytes)?.to_owned(),
                Ok(18) => msg.titles.push(r.read_message::<TitleSummary>(bytes)?),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for Genre {
    fn get_size(&self) -> usize {
        0
        + if self.name == String::default() { 0 } else { 1 + sizeof_len((&self.name).len()) }
        + self.titles.iter().map(|s| 1 + sizeof_len((s).get_size())).sum::<usize>()
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        if self.name != String::default() { w.write_with_tag(10, |w| w.write_string(&**&self.name))?; }
        for s in &self.titles { w.write_with_tag(18, |w| w.write_message(s))?; }
        Ok(())
    }
}

}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct SearchResponse {
    pub genres: Vec<mod_SearchResponse::Genre>,
    pub keywords: Vec<Tag>,
}

impl<'a> MessageRead<'a> for SearchResponse {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.genres.push(r.read_message::<mod_SearchResponse::Genre>(bytes)?),
                Ok(18) => msg.keywords.push(r.read_message::<Tag>(bytes)?),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for SearchResponse {
    fn get_size(&self) -> usize {
        0
        + self.genres.iter().map(|s| 1 + sizeof_len((s).get_size())).sum::<usize>()
        + self.keywords.iter().map(|s| 1 + sizeof_len((s).get_size())).sum::<usize>()
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        for s in &self.genres { w.write_with_tag(10, |w| w.write_message(s))?; }
        for s in &self.keywords { w.write_with_tag(18, |w| w.write_message(s))?; }
        Ok(())
    }
}

pub mod mod_SearchResponse {

use super::*;

#[derive(Debug, Default, PartialEq, Clone)]
pub struct Genre {
    pub tag: Option<Tag>,
    pub icon_image_url: String,
}

impl<'a> MessageRead<'a> for Genre {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.tag = Some(r.read_message::<Tag>(bytes)?),
                Ok(18) => msg.icon_image_url = r.read_string(bytes)?.to_owned(),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for Genre {
    fn get_size(&self) -> usize {
        0
        + self.tag.as_ref().map_or(0, |m| 1 + sizeof_len((m).get_size()))
        + if self.icon_image_url == String::default() { 0 } else { 1 + sizeof_len((&self.icon_image_url).len()) }
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        if let Some(ref s) = self.tag { w.write_with_tag(10, |w| w.write_message(s))?; }
        if self.icon_image_url != String::default() { w.write_with_tag(18, |w| w.write_string(&**&self.icon_image_url))?; }
        Ok(())
    }
}

}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct ResultResponse {
    pub titles: Vec<TitleSummary>,
}

impl<'a> MessageRead<'a> for ResultResponse {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.titles.push(r.read_message::<TitleSummary>(bytes)?),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for ResultResponse {
    fn get_size(&self) -> usize {
        0
        + self.titles.iter().map(|s| 1 + sizeof_len((s).get_size())).sum::<usize>()
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        for s in &self.titles { w.write_with_tag(10, |w| w.write_message(s))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct SearchRequest {
    pub words: String,
}

impl<'a> MessageRead<'a> for SearchRequest {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.words = r.read_string(bytes)?.to_owned(),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for SearchRequest {
    fn get_size(&self) -> usize {
        0
        + if self.words == String::default() { 0 } else { 1 + sizeof_len((&self.words).len()) }
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        if self.words != String::default() { w.write_with_tag(10, |w| w.write_string(&**&self.words))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct TitleSummary {
    pub id: u32,
    pub thumbnail_url: String,
    pub name: String,
    pub author_name: String,
    pub catch_copy: String,
    pub description: String,
    pub is_recently_update: bool,
    pub bookmark_count: u32,
    pub ticket_info: Option<TicketInfo>,
    pub remaining_rental_time: u32,
}

impl<'a> MessageRead<'a> for TitleSummary {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(8) => msg.id = r.read_uint32(bytes)?,
                Ok(18) => msg.thumbnail_url = r.read_string(bytes)?.to_owned(),
                Ok(26) => msg.name = r.read_string(bytes)?.to_owned(),
                Ok(34) => msg.author_name = r.read_string(bytes)?.to_owned(),
                Ok(42) => msg.catch_copy = r.read_string(bytes)?.to_owned(),
                Ok(50) => msg.description = r.read_string(bytes)?.to_owned(),
                Ok(808) => msg.is_recently_update = r.read_bool(bytes)?,
                Ok(816) => msg.bookmark_count = r.read_uint32(bytes)?,
                Ok(826) => msg.ticket_info = Some(r.read_message::<TicketInfo>(bytes)?),
                Ok(1608) => msg.remaining_rental_time = r.read_uint32(bytes)?,
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for TitleSummary {
    fn get_size(&self) -> usize {
        0
        + if self.id == 0u32 { 0 } else { 1 + sizeof_varint(*(&self.id) as u64) }
        + if self.thumbnail_url == String::default() { 0 } else { 1 + sizeof_len((&self.thumbnail_url).len()) }
        + if self.name == String::default() { 0 } else { 1 + sizeof_len((&self.name).len()) }
        + if self.author_name == String::default() { 0 } else { 1 + sizeof_len((&self.author_name).len()) }
        + if self.catch_copy == String::default() { 0 } else { 1 + sizeof_len((&self.catch_copy).len()) }
        + if self.description == String::default() { 0 } else { 1 + sizeof_len((&self.description).len()) }
        + if self.is_recently_update == false { 0 } else { 2 + sizeof_varint(*(&self.is_recently_update) as u64) }
        + if self.bookmark_count == 0u32 { 0 } else { 2 + sizeof_varint(*(&self.bookmark_count) as u64) }
        + self.ticket_info.as_ref().map_or(0, |m| 2 + sizeof_len((m).get_size()))
        + if self.remaining_rental_time == 0u32 { 0 } else { 2 + sizeof_varint(*(&self.remaining_rental_time) as u64) }
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        if self.id != 0u32 { w.write_with_tag(8, |w| w.write_uint32(*&self.id))?; }
        if self.thumbnail_url != String::default() { w.write_with_tag(18, |w| w.write_string(&**&self.thumbnail_url))?; }
        if self.name != String::default() { w.write_with_tag(26, |w| w.write_string(&**&self.name))?; }
        if self.author_name != String::default() { w.write_with_tag(34, |w| w.write_string(&**&self.author_name))?; }
        if self.catch_copy != String::default() { w.write_with_tag(42, |w| w.write_string(&**&self.catch_copy))?; }
        if self.description != String::default() { w.write_with_tag(50, |w| w.write_string(&**&self.description))?; }
        if self.is_recently_update != false { w.write_with_tag(808, |w| w.write_bool(*&self.is_recently_update))?; }
        if self.bookmark_count != 0u32 { w.write_with_tag(816, |w| w.write_uint32(*&self.bookmark_count))?; }
        if let Some(ref s) = self.ticket_info { w.write_with_tag(826, |w| w.write_message(s))?; }
        if self.remaining_rental_time != 0u32 { w.write_with_tag(1608, |w| w.write_uint32(*&self.remaining_rental_time))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct DetailResponse {
    pub id: u32,
    pub cover_image_url: String,
    pub name: String,
    pub author_name: String,
    pub update_info: String,
    pub tags: Vec<Tag>,
    pub description: String,
    pub notification: String,
    pub bookmark_count: u32,
    pub ticket_info: Option<TicketInfo>,
    pub external_links: Vec<mod_DetailResponse::ExternalLink>,
    pub chapters: Vec<ChapterSummary>,
    pub currency: Option<Currency>,
}

impl<'a> MessageRead<'a> for DetailResponse {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(8) => msg.id = r.read_uint32(bytes)?,
                Ok(18) => msg.cover_image_url = r.read_string(bytes)?.to_owned(),
                Ok(26) => msg.name = r.read_string(bytes)?.to_owned(),
                Ok(34) => msg.author_name = r.read_string(bytes)?.to_owned(),
                Ok(42) => msg.update_info = r.read_string(bytes)?.to_owned(),
                Ok(50) => msg.tags.push(r.read_message::<Tag>(bytes)?),
                Ok(58) => msg.description = r.read_string(bytes)?.to_owned(),
                Ok(66) => msg.notification = r.read_string(bytes)?.to_owned(),
                Ok(808) => msg.bookmark_count = r.read_uint32(bytes)?,
                Ok(818) => msg.ticket_info = Some(r.read_message::<TicketInfo>(bytes)?),
                Ok(826) => msg.external_links.push(r.read_message::<mod_DetailResponse::ExternalLink>(bytes)?),
                Ok(1602) => msg.chapters.push(r.read_message::<ChapterSummary>(bytes)?),
                Ok(2402) => msg.currency = Some(r.read_message::<Currency>(bytes)?),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for DetailResponse {
    fn get_size(&self) -> usize {
        0
        + if self.id == 0u32 { 0 } else { 1 + sizeof_varint(*(&self.id) as u64) }
        + if self.cover_image_url == String::default() { 0 } else { 1 + sizeof_len((&self.cover_image_url).len()) }
        + if self.name == String::default() { 0 } else { 1 + sizeof_len((&self.name).len()) }
        + if self.author_name == String::default() { 0 } else { 1 + sizeof_len((&self.author_name).len()) }
        + if self.update_info == String::default() { 0 } else { 1 + sizeof_len((&self.update_info).len()) }
        + self.tags.iter().map(|s| 1 + sizeof_len((s).get_size())).sum::<usize>()
        + if self.description == String::default() { 0 } else { 1 + sizeof_len((&self.description).len()) }
        + if self.notification == String::default() { 0 } else { 1 + sizeof_len((&self.notification).len()) }
        + if self.bookmark_count == 0u32 { 0 } else { 2 + sizeof_varint(*(&self.bookmark_count) as u64) }
        + self.ticket_info.as_ref().map_or(0, |m| 2 + sizeof_len((m).get_size()))
        + self.external_links.iter().map(|s| 2 + sizeof_len((s).get_size())).sum::<usize>()
        + self.chapters.iter().map(|s| 2 + sizeof_len((s).get_size())).sum::<usize>()
        + self.currency.as_ref().map_or(0, |m| 2 + sizeof_len((m).get_size()))
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        if self.id != 0u32 { w.write_with_tag(8, |w| w.write_uint32(*&self.id))?; }
        if self.cover_image_url != String::default() { w.write_with_tag(18, |w| w.write_string(&**&self.cover_image_url))?; }
        if self.name != String::default() { w.write_with_tag(26, |w| w.write_string(&**&self.name))?; }
        if self.author_name != String::default() { w.write_with_tag(34, |w| w.write_string(&**&self.author_name))?; }
        if self.update_info != String::default() { w.write_with_tag(42, |w| w.write_string(&**&self.update_info))?; }
        for s in &self.tags { w.write_with_tag(50, |w| w.write_message(s))?; }
        if self.description != String::default() { w.write_with_tag(58, |w| w.write_string(&**&self.description))?; }
        if self.notification != String::default() { w.write_with_tag(66, |w| w.write_string(&**&self.notification))?; }
        if self.bookmark_count != 0u32 { w.write_with_tag(808, |w| w.write_uint32(*&self.bookmark_count))?; }
        if let Some(ref s) = self.ticket_info { w.write_with_tag(818, |w| w.write_message(s))?; }
        for s in &self.external_links { w.write_with_tag(826, |w| w.write_message(s))?; }
        for s in &self.chapters { w.write_with_tag(1602, |w| w.write_message(s))?; }
        if let Some(ref s) = self.currency { w.write_with_tag(2402, |w| w.write_message(s))?; }
        Ok(())
    }
}

pub mod mod_DetailResponse {

use super::*;

#[derive(Debug, Default, PartialEq, Clone)]
pub struct ExternalLink {
    pub description: String,
    pub image_url: String,
    pub external_link_url: String,
}

impl<'a> MessageRead<'a> for ExternalLink {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.description = r.read_string(bytes)?.to_owned(),
                Ok(18) => msg.image_url = r.read_string(bytes)?.to_owned(),
                Ok(26) => msg.external_link_url = r.read_string(bytes)?.to_owned(),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for ExternalLink {
    fn get_size(&self) -> usize {
        0
        + if self.description == String::default() { 0 } else { 1 + sizeof_len((&self.description).len()) }
        + if self.image_url == String::default() { 0 } else { 1 + sizeof_len((&self.image_url).len()) }
        + if self.external_link_url == String::default() { 0 } else { 1 + sizeof_len((&self.external_link_url).len()) }
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        if self.description != String::default() { w.write_with_tag(10, |w| w.write_string(&**&self.description))?; }
        if self.image_url != String::default() { w.write_with_tag(18, |w| w.write_string(&**&self.image_url))?; }
        if self.external_link_url != String::default() { w.write_with_tag(26, |w| w.write_string(&**&self.external_link_url))?; }
        Ok(())
    }
}

}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct ChunkResponse {
    pub subject: String,
    pub titles: Vec<TitleSummary>,
}

impl<'a> MessageRead<'a> for ChunkResponse {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.subject = r.read_string(bytes)?.to_owned(),
                Ok(18) => msg.titles.push(r.read_message::<TitleSummary>(bytes)?),
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for ChunkResponse {
    fn get_size(&self) -> usize {
        0
        + if self.subject == String::default() { 0 } else { 1 + sizeof_len((&self.subject).len()) }
        + self.titles.iter().map(|s| 1 + sizeof_len((s).get_size())).sum::<usize>()
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        if self.subject != String::default() { w.write_with_tag(10, |w| w.write_string(&**&self.subject))?; }
        for s in &self.titles { w.write_with_tag(18, |w| w.write_message(s))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct Request {
    pub title_id: u32,
}

impl<'a> MessageRead<'a> for Request {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(8) => msg.title_id = r.read_uint32(bytes)?,
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for Request {
    fn get_size(&self) -> usize {
        0
        + if self.title_id == 0u32 { 0 } else { 1 + sizeof_varint(*(&self.title_id) as u64) }
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        if self.title_id != 0u32 { w.write_with_tag(8, |w| w.write_uint32(*&self.title_id))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct RequestChunk {
    pub type_pb: mod_RequestChunk::Type,
    pub id: u32,
}

impl<'a> MessageRead<'a> for RequestChunk {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(8) => msg.type_pb = r.read_enum(bytes)?,
                Ok(16) => msg.id = r.read_uint32(bytes)?,
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for RequestChunk {
    fn get_size(&self) -> usize {
        0
        + if self.type_pb == cat::mod_RequestChunk::Type::TAG { 0 } else { 1 + sizeof_varint(*(&self.type_pb) as u64) }
        + if self.id == 0u32 { 0 } else { 1 + sizeof_varint(*(&self.id) as u64) }
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        if self.type_pb != cat::mod_RequestChunk::Type::TAG { w.write_with_tag(8, |w| w.write_enum(*&self.type_pb as i32))?; }
        if self.id != 0u32 { w.write_with_tag(16, |w| w.write_uint32(*&self.id))?; }
        Ok(())
    }
}

pub mod mod_RequestChunk {


#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum Type {
    TAG = 0,
    SECTION = 1,
}

impl Default for Type {
    fn default() -> Self {
        Type::TAG
    }
}

impl From<i32> for Type {
    fn from(i: i32) -> Self {
        match i {
            0 => Type::TAG,
            1 => Type::SECTION,
            _ => Self::default(),
        }
    }
}

impl<'a> From<&'a str> for Type {
    fn from(s: &'a str) -> Self {
        match s {
            "TAG" => Type::TAG,
            "SECTION" => Type::SECTION,
            _ => Self::default(),
        }
    }
}

}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct UpdatesResponse {
    pub titles: Vec<TitleSummary>,
    pub is_first_title_highlighted: bool,
}

impl<'a> MessageRead<'a> for UpdatesResponse {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(10) => msg.titles.push(r.read_message::<TitleSummary>(bytes)?),
                Ok(16) => msg.is_first_title_highlighted = r.read_bool(bytes)?,
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for UpdatesResponse {
    fn get_size(&self) -> usize {
        0
        + self.titles.iter().map(|s| 1 + sizeof_len((s).get_size())).sum::<usize>()
        + if self.is_first_title_highlighted == false { 0 } else { 1 + sizeof_varint(*(&self.is_first_title_highlighted) as u64) }
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        for s in &self.titles { w.write_with_tag(10, |w| w.write_message(s))?; }
        if self.is_first_title_highlighted != false { w.write_with_tag(16, |w| w.write_bool(*&self.is_first_title_highlighted))?; }
        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct UpdatesRequest {
    pub day: mod_UpdatesRequest::Day,
}

impl<'a> MessageRead<'a> for UpdatesRequest {
    fn from_reader(r: &mut BytesReader, bytes: &'a [u8]) -> Result<Self> {
        let mut msg = Self::default();
        while !r.is_eof() {
            match r.next_tag(bytes) {
                Ok(8) => msg.day = r.read_enum(bytes)?,
                Ok(t) => { r.read_unknown(bytes, t)?; }
                Err(e) => return Err(e),
            }
        }
        Ok(msg)
    }
}

impl MessageWrite for UpdatesRequest {
    fn get_size(&self) -> usize {
        0
        + if self.day == cat::mod_UpdatesRequest::Day::MON { 0 } else { 1 + sizeof_varint(*(&self.day) as u64) }
    }

    fn write_message<W: Write>(&self, w: &mut Writer<W>) -> Result<()> {
        if self.day != cat::mod_UpdatesRequest::Day::MON { w.write_with_tag(8, |w| w.write_enum(*&self.day as i32))?; }
        Ok(())
    }
}

pub mod mod_UpdatesRequest {


#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum Day {
    MON = 0,
    TUE = 1,
    WED = 2,
    THU = 3,
    FRI = 4,
    SAT = 5,
    SUN = 6,
}

impl Default for Day {
    fn default() -> Self {
        Day::MON
    }
}

impl From<i32> for Day {
    fn from(i: i32) -> Self {
        match i {
            0 => Day::MON,
            1 => Day::TUE,
            2 => Day::WED,
            3 => Day::THU,
            4 => Day::FRI,
            5 => Day::SAT,
            6 => Day::SUN,
            _ => Self::default(),
        }
    }
}

impl<'a> From<&'a str> for Day {
    fn from(s: &'a str) -> Self {
        match s {
            "MON" => Day::MON,
            "TUE" => Day::TUE,
            "WED" => Day::WED,
            "THU" => Day::THU,
            "FRI" => Day::FRI,
            "SAT" => Day::SAT,
            "SUN" => Day::SUN,
            _ => Self::default(),
        }
    }
}

}

